<?php

class User {
  public $id;
  public $username;
  public $name;
  public $email;
  protected $password;

  function User($id, $username, $name, $email) {
    $this->id = $id;
    $this->username = $username;
    $this->name = $name;
    $this->email = $email;
  }

  public static function all() {
    global $CONFIG;
    $list = [];
    $db = Database::getInstance($CONFIG);
    $result = $db->fetchAll('SELECT * from users');

    foreach($result as $user) {
      $list[] = new User($user->id, $user->username, $user->name, $user->email);
    }

    return $list;
  }

  public static function find($id) {
    global $CONFIG;
    $db = Database::getInstance($CONFIG);
    $result = $db->fetchObject("SELECT * from users WHERE id='$id'");

    return $result;
  }
}

?>
