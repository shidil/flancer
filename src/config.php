<?php

global $CONFIG;

$CONFIG = (object)[
  "host" => "localhost",
  "database" => "flancer",
  "password" => "password",
  "username" => "root",
  "fileRoot" =>  __DIR__.'/',
  "domain" => "http://localhost/flancer"
];

?>
