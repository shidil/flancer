<?php

class UsersController {
  public function index() {
    $users = User::all();
    require_once(__DIR__.'/../views/users/index.php');
  }

  public function error() {
    require_once(__DIR__.'/../views/users/error.php');
  }
}

?>
