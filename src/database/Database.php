<?php

/**
 * Creates and maintains the connection to the database.
 * Performs basic database operations.
 */
class Database {
  // holds the mysqli connection link object
  public $connection;
  protected static $instance;

  function Database($config) {
    $this -> connection = new mysqli($config->host, $config->username, $config->password, $config->database);
    if ($this -> connection -> connect_errno) {
      die("Failed to connect to MySQLi: " . $this -> connection -> connect_error);
    }
  }

  public static function getInstance() {
    global $CONFIG;

    if (isset(self::$instance)) {
      return self::$instance;
    } else {
      self::$instance = new Database($CONFIG);
      return self::$instance;
    }
  }

  private function exec($query) {
  }

  public function rows($query) {
    $result = $this -> connection -> query($query);
    return $result -> num_rows;
  }

  public function query($query) {
    return $this -> connection -> query($query);
  }

  public function fetchObject($query) {
            //echo $query;
    $result = $this -> connection -> query($query);
    $obj = $result -> fetch_object();
    return $obj;
  }

  public function fetchAll($query) {
    $list = [];
    $result = $this ->connection->query($query);

    while ($row = $result->fetch_object()) {
      $list[] = $row;
    }

    $result->close();
    return $list;
  }

  /**
   * Inserts into specified table in database with provided values for fields
   * @param  [type] $feilds [description]
   * @param  [type] $data   [description]
   * @param  [type] $table  [description]
   * @return [type]         [description]
   */
  public function insert($table, $feilds, $data) {
    $cols = implode(',', array_values($feilds));

    foreach (array_values($data) as $value) {
      isset($vals) ? $vals .= ',' : $vals = '';
      $vals .= '\'' . $this -> connection -> real_escape_string($value) . '\'';
    }

    return $this -> connection -> real_query('INSERT INTO ' . $table . ' (' . $cols . ') VALUES (' . $vals . ')');
  }

  /**
   * [update description]
   * @param  [type] $table  [description]
   * @param  [type] $feilds [description]
   * @param  [type] $values [description]
   * @param  [type] $condition  [description]
   * @return [type]         [description]
   */
  public function update($table, $feilds, $values, $condition) {

    $query='UPDATE `'.$table.'` SET ';

    foreach ($feilds as $i => $value) {
      $query.='`'.$feilds[$i].'`=\''.$this -> connection -> real_escape_string($values[$i]).'\',';
    }

    $query=  rtrim($query, ',');
    $query.='  WHERE '.$condition;
                //echo $query;
    return $this -> connection -> query($query);

  }

  public function close(){
    $this->connection->close();
  }

  public function escape($value){
    return $this->connection->escape_string($value);
  }
}

?>
